from django.conf.urls import include, url
from django.contrib import admin
#below are added after initial working commit
from django.contrib.auth import views as auth_views
from django.conf.urls import *

urlpatterns = [
    # Examples:
    # url(r'^$', 'LoginApp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    #url(r'^login/$', 'userPanel.views.login_user'),
    #below are added after initial working commit
    #url(r'^admin/', include('django.contrib.admin.urls')),
    url(r'^accounts/', include('registration.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^login/$',auth_views.login,{'template_name': 'login.html'},name='auth_login'),
    url(r'^logout/$',auth_views.logout,{'template_name': 'logout.html'},name='auth_logout'),
    url(r'^password/change/$',auth_views.password_change,{'template_name': 'password_change_form.html'},name='auth_password_change'),
    url(r'^password/change/done/$',auth_views.password_change_done,{'template_name': 'password_change_done.html'},name='auth_password_change_done'),
    url(r'^password/reset/$',auth_views.password_reset,{'template_name': 'password_reset_form.html'},name='auth_password_reset'),
    url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',auth_views.password_reset_confirm,{'template_name': 'password_reset_confirm.html'},name='auth_password_reset_confirm'),
    url(r'^password/reset/complete/$',auth_views.password_reset_complete,{'template_name': 'password_reset_complete.html'},name='auth_password_reset_complete'),
    url(r'^password/reset/done/$',auth_views.password_reset_done,{'template_name': 'password_reset_done.html'},name='auth_password_reset_done'),
]
